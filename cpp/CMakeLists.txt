cmake_minimum_required (VERSION 3.0)

project (dupkiller)

set (CMAKE_CXX_FLAGS "-Wall -std=c++11 -O3")

#add_definitions (-DUSE_BOOST_MMAP_IMPL)
#add_definitions (-DUSE_STREAMS_IMPL)
#add_definitions (-DUSE_BUFFERS_IMPL)

add_executable (
	dupkiller
	src/main.cpp
	src/futils.cpp
)

target_link_libraries (
	dupkiller
	boost_system
	boost_program_options
	boost_filesystem
)
