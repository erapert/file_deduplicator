#pragma once
#include <map>
#include <vector>
#include <string>
#include <exception>

namespace FUtils {
	
	// TODO: parameterize these exceptions so we can see what file path caused the exception
	class FailedToStatFileException : public std::exception {
		virtual const char * what () const throw () {
			return "Failed to stat file.";
		}
	};
	
	class FailedToOpenFileException : public std::exception {
		virtual const char * what () const throw () {
			return "Failed to open file.";
		}
	};
	
	class FailedToDeleteFileException : public std::exception {
		virtual const char * what () const throw () {
			return "Failed to delete file.";
		}
	};
	
	// path1, path2: paths to files
	// returns: true if the files are the same stat size, false if either can't be opened or if not equal
	bool fileSizesAreSame (const std::string & path1, const std::string & path2);
	
	// path1, path2: paths to files
	// returns: true if the files are equal, false if either file can't be opened or if they're not equal
	bool filesAreEqual (const std::string & path1, const std::string & path2);
	
	// paths: list of file paths
	// returns a map of files => vector of file paths that are duplicates of the key
	std::map<std::string, std::vector<std::string>> findDuplicates (std::vector<std::string> & paths);
	
	// path: full path to a directory
	// returns: list of all files (recursive) inside of path
	std::vector<std::string> listAllFiles (const std::string & path);
	
	// duplicates: a map of files and their duplicates
	// returns: number of files deleted
	unsigned int deleteDuplicates (const std::map <std::string, std::vector<std::string>> & duplicates);
}

