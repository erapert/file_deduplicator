#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

// requires that you pass "-lboost_program_options" to g++ (you'll need boost installed of course)
#include <boost/program_options.hpp>
namespace po = boost::program_options;

// requires that you pass "-lboost_file_system" to g++ (you'll need boost installed of course)
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include "futils.hpp"


int main (int argc, char ** argv) {
	po::options_description opts ("Usage");
	po::variables_map args;
	
	opts.add_options ()
		("help", "show this help message")
		("path", po::value<string>(), "check all files under this path (recursively)")
		("list", "list mode-- merely list files that are duplicates; does not delete")
		("kill", "kill mode-- delete duplicates; does nothing if list-mode specified")
		("stats", "print how long it took, path size before and after, etc.");
	
	try {
		po::store (po::parse_command_line (argc, argv, opts), args);
		po::notify (args);
	} catch (...) {
		cout << opts << endl;
		return 1;
	}
	
	if (!args.count ("path")) {
		cout << opts << endl;
		return 1;
	}
	
	auto targetPath = args["path"].as<string>();
	
	if (!fs::exists (targetPath)) {
		cout << "Path doesn't exist: \"" << targetPath << "\"" << endl;
		return 1;
	}
	
	if (!fs::is_directory (targetPath)) {
		cout << "Path must be a directory not a regular file: \"" << targetPath << "\"" << endl;
		return 1;
	}
	
	cout << "Finding all files in \"" << targetPath << "\"..." << endl;
	auto fileList = FUtils::listAllFiles (targetPath);
	
	cout << "Sorting alphabetically..." << endl;
	sort (fileList.begin(), fileList.end());
	
	cout << "Finding duplicates..." << endl;
	auto dupsList = FUtils::findDuplicates (fileList);
	
	
	if (args.count ("list")) {
		cout << "Duplicates:" << endl;
		for (auto & kv : dupsList) {
			cout << "\"" << kv.first << "\":" << endl;
			for (auto & p : kv.second) {
				cout << "\t\"" << p << "\"" << endl;
			}
		}
	} else if (args.count ("kill")) {
		cout << "Deleting duplicates..." << endl;
		auto numDeleted = FUtils::deleteDuplicates (dupsList);
	}
	
	// if (args.count ("stats")) {
	//		TODO: print summary of before and after path stats (size, num files, num dupes, num deleted)
	//		cout << "\tDeleted " << numDeleted << " files." << endl;
	// }
	
	cout << "Done." << endl;
	return 0;
}
