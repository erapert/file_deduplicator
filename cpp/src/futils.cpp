#include "futils.hpp"

#include <fstream>
#include <cstring>
#include <sys/stat.h>
#include <cstdio>

// requires that you pass "-lboost_file_system" to g++ (you'll need boost installed of course)
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#if defined(USE_BOOST_MMAP_IMPL) || defined(USE_STREAMS_IMPL)
	// provides std::equal()
	#include <algorithm>
#endif

#ifdef USE_BOOST_MMAP_IMPL
	#include <boost/iostreams/device/mapped_file.hpp>
	namespace bio = boost::iostreams;
#endif

using namespace std;

namespace FUtils {
	
	
	bool fileSizesAreSame (const string & path1, const string & path2) {
		struct stat p1stat;
		struct stat p2stat;
		
		if (lstat (path1.c_str(), &p1stat) == -1) {
			throw FUtils::FailedToStatFileException ();
		}
		if (lstat (path2.c_str(), &p2stat) == -1) {
			throw FUtils::FailedToStatFileException ();
		}
		
		return (p1stat.st_size == p2stat.st_size);
	}
	
	#ifdef USE_BOOST_MMAP_IMPL
		// boost's memory mapping is supposed to be ultra fast...
		bool filesAreEqual (const string & path1, const string & path2) {
			if (!fileSizesAreSame (path1, path2)) {
				return false;
			}
			
			auto first = bio::mapped_file_source { path1.c_str() };
			auto second = bio::mapped_file_source { path2.c_str() };
			
			if (first.size() == second.size()) {
				return std::equal (first.data(), first.data() + first.size(), second.data());
			}
			
			return false;
		}
	
	#elif USE_STREAMS_IMPL
		// streams are relatively slow...
		bool filesAreEqual (const string & path1, const string & path2) {
			if (!fileSizesAreSame (path1, path2)) {
				return false;
			}
			
			auto first = ifstream (path1, std::ifstream::in | std::ifstream::binary);
			auto second = ifstream (path2, std::ifstream::in | std::ifstream::binary);
			
			if (!first.is_open () || !second.is_open ()) {
				return false;
			}
			
			// todo: check file size
			
			return std::equal (
				istreambuf_iterator<char> (first),
				istreambuf_iterator<char> (),
				istreambuf_iterator<char> (second)
			);
		}
	#elif USE_INTERNAL_BUFFERS_IMPL
		
		#define BUFFSIZE 1000000
		// using buffers is not as fast as boost mmap
		// but still much faster than streams with no dependency on boost
		bool filesAreEqual (const string & path1, const string & path2) {
			if (!fileSizesAreSame (path1, path2)) {
				return false;
			}
			
			auto first = ifstream (path1, std::ifstream::in | std::ifstream::binary);
			auto second = ifstream (path2, std::ifstream::in | std::ifstream::binary);
			
			if (!first.is_open () || !second.is_open ()) {
				return false;
			}
			
			auto firstBuff = new char [BUFFSIZE];
			auto secondBuff = new char [BUFFSIZE];
			
			do {
				first.read (firstBuff, BUFFSIZE);
				second.read (secondBuff, BUFFSIZE);
				auto lenRead = first.gcount ();
				
				if (std::memcmp (firstBuff, secondBuff, lenRead) != 0) {
					delete [] firstBuff;
					delete [] secondBuff;
					return false;
				}
			} while (first.good () || second.good ());
			
			delete [] firstBuff;
			delete [] secondBuff;
			return true;
		}
		
	#else
		
		bool filesAreEqual (const string & path1, const string & path2, char * firstBuff, char * secondBuff, unsigned int buffSize) {
			if (!fileSizesAreSame (path1, path2)) {
				return false;
			}
			
			auto first = ifstream (path1, std::ifstream::in | std::ifstream::binary);
			auto second = ifstream (path2, std::ifstream::in | std::ifstream::binary);
			
			if (!first.is_open () || !second.is_open ()) {
				return false;
			}
			
			do {
				first.read (firstBuff, buffSize);
				second.read (secondBuff, buffSize);
				auto lenRead = first.gcount ();
				
				if (std::memcmp (firstBuff, secondBuff, lenRead) != 0) {
					return false;
				}
			} while (first.good () || second.good ());
			
			return true;
		}
	#endif
	
	std::map<string, vector<string>> findDuplicates (std::vector<std::string> & paths) {
		auto rtrn = map<string, vector<string>>();
		
		const auto buffSize = 1000000;
		auto b1 = new char [buffSize];
		auto b2 = new char [buffSize];
		
		for (unsigned int i = 0; i < paths.size(); i++) {
			auto p1 = paths [i];
			
			if (p1 != "") {
				for (unsigned int j = i + 1; j < paths.size(); j++) {
					auto p2 = paths [j];
					
					if (p2 != "" && FUtils::filesAreEqual (p1, p2, b1, b2, buffSize)) {
						rtrn [p1].push_back (p2);
						paths [j] = "";
					}
				}
			}
		}
		
		delete [] b1;
		delete [] b2;
		
		return rtrn;
	}
	
	std::vector<std::string> listAllFiles (const std::string & path) {
		auto rtrn = vector<string>();
		
		for (auto entry : fs::recursive_directory_iterator (path)) {
			auto entryPath = entry.path();
			if (fs::is_regular_file (entryPath)) {
				rtrn.push_back (entryPath.generic_string());
			}
		}
		
		return rtrn;
	}
	
	unsigned int deleteDuplicates (const map <string, vector<string>> & duplicates) {
		auto rtrn = 0U;
		for (auto & kv : duplicates) {
			for (auto & path : kv.second) {
				// std::remove returns 0 on success, non-zero on failure
				if (std::remove (path.c_str ()) != 0) {
					throw FUtils::FailedToDeleteFileException ();
				}
				rtrn++;
			}
		}
		return rtrn;
	}
}
